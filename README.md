# Registry Module

This module is capable to generate a registry to store docker images.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - slug and full name

Usage
-----

```hcl
module "registry" {
  source      = "git::https://gitlab.com/mesabg-tfmodules/registry.git"

  environment = "environment"

  name = {
    slug      = "slugname"
    full      = "Full Name"
  }
}
```

Outputs
=======

 - `registry` - Created Registry


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
