variable "environment" {
  type = string
  description = "Environment name"
}

variable "name" {
  type = object({
    slug = string
    full = string
  })
  description = "General registry name."
}
