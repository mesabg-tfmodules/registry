terraform {
  required_version = ">= 0.13.2"
}

resource "aws_ecr_repository" "ecr" {
  name                            = var.name.slug

  image_scanning_configuration {
    scan_on_push                  = true
  }

  tags = {
    Name                          = var.name.full
    Environment                   = var.environment
  }
}

resource "aws_ecr_lifecycle_policy" "ecr_lifecycle_policy" {
  repository  = aws_ecr_repository.ecr.name
  policy      = <<EOF
  {
    "rules": [
      {
        "rulePriority": 1,
        "description": "Expire images when more than 10 on repository",
        "selection": {
          "tagStatus": "any",
          "countType": "imageCountMoreThan",
          "countNumber": 10
        },
        "action": {
          "type": "expire"
        }
      }
    ]
  }
  EOF
}
